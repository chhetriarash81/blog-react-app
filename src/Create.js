import React, { useState } from 'react'
import {useHistory} from 'react-router-dom'

const Create = () => {
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const [author, setAuthor] = useState('jhon');
    const [isPending, setIsPending] = useState(false)
    const history = useHistory();

    const handleSubmit = (e) => {
        e.preventDefault();
        const blog = {title, body, author};
        setIsPending(true)

        fetch('http://localhost:8000/blogs', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(blog)
        }).then(() => {
            console.log('New Blog Added')
            setIsPending(false)
            history.push('/')
        })
    }

    return(
        <div className="create">
            <h2>Add New a Post</h2>
            <form onSubmit={handleSubmit}>
                <label>Blog Title: </label>
                <input
                    type="text"
                    value={title}
                    onChange={(e)=> setTitle(e.target.value)}
                    required
                />

                <label>Blog Body :</label>
                <textarea
                    value={body}
                    onChange={(e) => setBody(e.target.value)}
                    required
                ></textarea>

                <label>Blog Author: </label>
                <select
                    value={author}
                    onChange={(e) => setAuthor(e.target.value)}
                >
                    <option value="Jhon">Jhon</option>
                    <option value="Ashok">Ashok</option>
                </select>

                { !isPending && <button>Add New Blog</button>}
                { isPending && <button disabled>Adding Blog...</button>}
            </form>
        </div>
    )
}

export default Create